using System;

namespace LettingWeb.Models
{
    public class PropertyModel
    {
        public PropertyModel() { }
        public PropertyModel(string Addr, string Image, double Price, string Desc)
        {
            this.Address = Addr;
            this.Image = Image;
            this.Price = Price;
            this.Description = Desc;
        }

        public int Id { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }
        public string Image { get; set; }
        public double Price { get; set; }
    }
}
