using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LettingWeb.Models
{
    public class FavouritesModel
    {
        public FavouritesModel() { }
        public FavouritesModel(string UserId, int PropertyId) {
            this.UserId = UserId;
            this.PropertyId = PropertyId;
        }

        [Key, Column(Order = 0)]
        public string UserId { get; set; }

        [Key, Column(Order = 1)]
        public int PropertyId { get; set; }
    }
}
