﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using LettingWeb.Models;
using Microsoft.Extensions.Configuration;
using LettingWeb.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;

namespace LettingWeb.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PropertyController : ControllerBase
    {
        private readonly ILogger<PropertyController> _logger;
        private readonly IConfiguration _config;

        public PropertyController(ILogger<PropertyController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _config = configuration;
        }

        [HttpGet("get_all")]
        public IActionResult get_all()
        {
            using (var conn = getDbContext())
            {
                List<PropertyModel> properties = conn.Property.ToList();
                Dictionary<string, object> resp = new Dictionary<string, object>() { { "status", "ok" }, { "payload", properties } };
                return StatusCode(200, resp);
            }
        }

        [Authorize]
        [HttpGet("new_property/{addr}/{desc}/{price:double}/{img}")]       //This endpoint is purely for quick testing - would not be implemented like this for production!
        public IActionResult add_property(string addr, string desc, double price, string img)
        {
            using (var conn = getDbContext())
            {
                PropertyModel property = new PropertyModel(addr, img, price, desc);
                conn.Property.Add(property);
                conn.SaveChanges();
                return CreatedAtAction("new_property", property);
            }
        }

        [Authorize]
        [HttpGet("get_favourites")]
        public IActionResult get_favourites()
        {
            using (var conn = getDbContext())
            {
                List<object> keys = new List<object>();
                string userId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
                IQueryable<FavouritesModel> models = conn.Favourites.Where(f => f.UserId.Equals(userId));
                List<PropertyModel> properties = new List<PropertyModel>();

                foreach (FavouritesModel fmodel in models) { 
                    properties.Add(conn.Property.Where(p => p.Id.Equals(fmodel.PropertyId)).First());   //This is probably ineffient and error-prone, should figure out inner joins properly
                }

                Dictionary<string, object> resp = new Dictionary<string, object>() { { "status", "ok" }, { "payload", properties } };
                return StatusCode(200, resp);
            }
        }

        [Authorize]
        [HttpGet("toggle_favourite/{propId:int}")]
        public IActionResult toggle_favourite(int propId)
        {
            using (var conn = getDbContext())
            {
                List<object> keys = new List<object>();
                string userId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
                keys.Add(userId);
                keys.Add(propId);

                FavouritesModel res = conn.Favourites.Find(userId, propId);
                if (res == null)
                {
                    conn.Favourites.Add(new FavouritesModel(userId, propId));
                }
                else {
                    conn.Favourites.Remove(res);
                }
                conn.SaveChanges();
                Dictionary<string, object> resp = new Dictionary<string, object>() { { "status", "ok" }, };
                return StatusCode(200, resp);
            }
        }

        private ApplicationDbContext getDbContext()
        {
            return new ApplicationDbContext(new DbContextOptionsBuilder<ApplicationDbContext>().UseSqlServer(
                    _config.GetConnectionString("DefaultConnection")).Options);
        }
    }
}
