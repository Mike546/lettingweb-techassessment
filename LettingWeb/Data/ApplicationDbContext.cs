﻿using System;
using System.Collections.Generic;
using System.Text;
using LettingWeb.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace LettingWeb.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public DbSet<PropertyModel> Property { get; set; }
        public DbSet<FavouritesModel> Favourites { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<FavouritesModel>().HasKey(favourites => new { favourites.UserId, favourites.PropertyId });
        }
    }
}
